/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class WhitelistViewController: NSViewController {

    private let notification = Notification.Name(rawValue: "\(Constants.safariToolbarIdentifier).whitelist")
    private let whitelistItemDataSource = WhitelistItemDataSource()
    private let filterListManager = FilterlistManager()
    private var whitelistedArray = [String]()

    @IBOutlet weak var whitelistTitle: NSTextFieldCell!
    @IBOutlet weak var urlTextField: NSTextField!
    @IBOutlet weak var addWebsiteButton: NSButton!
    @IBOutlet weak var whitelistCollectionView: NSCollectionView!

    @IBAction func removeFromWhitelistButton(_ sender: NSButton) {
        // Get URL from sender's cell.
        let indexPath = IndexPath(item: sender.tag, section: 0)
        let cell = whitelistCollectionView.item(at: indexPath)
        guard let url = cell?.textField?.stringValue else { return }

        whitelistItemDataSource.removeURLfromArray(url: url) { [weak self] wlArray in
            self?.whitelistedArray = wlArray
            self?.filterListManager.reloadContentBlocker()
            self?.whitelistCollectionView.reloadData()
            FabricWrapper.sendEvent(event: "Whitelist View - URL Removed from Whitelist")
        }
    }

    @IBAction func urlTextFieldEntered(_ sender: NSTextField) {

        // If this print statement is removed, for some reason, `addWebsiteToWhitelist` func does not work ¯\_(ツ)_/¯
        print(sender.stringValue)

        // Disable UI Elements
        sender.isEnabled = false
        addWebsiteButton.isEnabled = false

        whitelistItemDataSource.getWhitelistArray { [weak self] whitelistArray in

            // Format hostname string
            guard !sender.stringValue.isEmpty,
                let hostname = NSString(string: sender.stringValue).whitelistedHostname(),
                !whitelistArray.contains(hostname) else {
                    sender.isEnabled = true
                    sender.stringValue = ""
                    self?.addWebsiteButton.isEnabled = true
                    return
            }

            self?.whitelistItemDataSource.appendToWhitelist(hostname: hostname) { whitelistArray in
                self?.whitelistedArray = whitelistArray
                // Reload NSCollectionView
                self?.whitelistCollectionView.reloadData()

                // Enable UI Elements
                sender.stringValue = ""
                sender.isEnabled = true
                self?.addWebsiteButton.isEnabled = true
                FabricWrapper.sendEvent(event: "Whitelist View - URL Added to Whitelist")
                self?.filterListManager.reloadContentBlocker()
            }
        }
    }

    @IBAction func addWebsiteToWhitelist(_ sender: NSButton) {
        urlTextFieldEntered(urlTextField)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        DistributedNotificationCenter.default().addObserver(self,
                                                            selector: #selector(self.updateWhitelist),
                                                            name: notification,
                                                            object: Constants.safariToolbarIdentifier)
        whitelistItemDataSource.getWhitelistArray { [weak self] whitelistedArray in
            self?.whitelistedArray = whitelistedArray
            self?.whitelistCollectionView.reloadData()
        }
    }

    override func viewWillAppear() {
        super.viewWillAppear()
        whitelistTitle.title = "Whitelist".localized

        FabricWrapper.sendEvent(event: "Whitelist View - Show Screen")
    }

    @objc
    func updateWhitelist() {
        whitelistCollectionView.reloadData()
    }
}

// MARK: - Extends ViewController to act as data source for the whitelist NSCollectionView
extension WhitelistViewController: NSCollectionViewDataSource {

    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return whitelistedArray.count
    }

    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let whitelistItem = NSUserInterfaceItemIdentifier(rawValue: Constants.whitelistItem)

        //swiftlint:disable:next force_cast
        let item = collectionView.makeItem(withIdentifier: whitelistItem, for: indexPath) as! WhitelistItem
        item.textField?.stringValue = whitelistedArray[indexPath.item]
        item.trashcanOutlet.tag = indexPath.item
        item.trashcanOutlet.isHidden = true

        return item
    }

}
