/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa

class HelpViewController: NSViewController {

    @IBOutlet weak var helpTitle: NSTextFieldCell!

    override func viewWillAppear() {
        super.viewWillAppear()
        helpTitle.title = "Help".localized

        FabricWrapper.sendEvent(event: "Help View - Show Screen")
    }

    @IBAction func openBugReportURL(_ sender: NSButton) {
        NSWorkspace.shared.open(URL(string: Constants.bugReportURL)!)
        FabricWrapper.sendEvent(event: "Help View - Open Bug Report")
    }
    @IBAction func openForumURL(_ sender: NSButton) {
        NSWorkspace.shared.open(URL(string: Constants.forumURL)!)
        FabricWrapper.sendEvent(event: "Help View - Open Forum")
    }
    @IBAction func openEmailSupportURL(_ sender: NSButton) {
        NSWorkspace.shared.open(URL(string: Constants.supportEmailURL)!)
        FabricWrapper.sendEvent(event: "Help View - Open Email Support")
    }
    @IBAction func openFacebookURL(_ sender: NSButton) {
        NSWorkspace.shared.open(URL(string: Constants.facebookURL)!)
    }
    @IBAction func openTwitterURL(_ sender: NSButton) {
        NSWorkspace.shared.open(URL(string: Constants.twitterURL)!)
    }
}
