/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation
import ABPKit

class FilterlistManager {

    var onExtensionReloading: () -> Void = { }
    var onExtensionReloadCompleted: (_ error: Error?) -> Void = { _ in }

    let extensionUpdater = SafariExtensionComposer.makeRulesUpdater(clientAppBundle: Bundle(for: FilterlistManager.self))

    /// Shared UserDefaults
    private let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)

    /// - Parameter language: The language variant of the filterlist to be returned
    /// - Returns: A filterlist for the selected language, or the default filterlist if a language variant cannot be found.
    func getFilterlistWithLanguage(_ language: FilterlistLanguage) -> Filterlist {
        let filterlists = Filterlists.filterlists
        return filterlists.first(where: { $0.language == language }) ?? filterlists[0]
    }

    func updateToLanguage(_ language: FilterlistLanguage) {
        updateExtension(withSource: sourceForLanguage(language, aaState: isAAOn()))
        storeSelectedLanguage(language)
    }

    func updateToAAState(_ isOn: Bool) {
        updateExtension(withSource: sourceForLanguage(getSelectedLanguage(), aaState: isOn))
        storeAAState(isOn)
    }

    func reloadContentBlocker() {
        updateExtension(withSource: sourceForLanguage(getLocalFilterListLanguage(), aaState: isAAOn()))
    }

    // MARK: - Private methods

    private func sourceForLanguage(_ language: FilterlistLanguage, aaState: Bool) -> RulesSource {
        let filterListForLanguage = getFilterlistWithLanguage(language)
        let list: BundledSource = aaState ? .withAA(.custom(filterListForLanguage.files[1])) : .withoutAA(.custom(filterListForLanguage.files[0]))

        return .bundled(list)
    }

    private func isAAOn() -> Bool {
        groupDefaults?.object(forKey: Constants.acceptableAdsEnabled) as? Bool ?? false
    }

    private func getSelectedLanguage() -> FilterlistLanguage {
        if let selectedFilterList = groupDefaults?.string(forKey: Constants.selectedFilterList),
            let language = FilterlistLanguage(rawValue: selectedFilterList) {
            return language
        }
        return getLocalFilterListLanguage()
    }

    private func updateExtension(withSource source: RulesSource) {
        onExtensionReloading()
        extensionUpdater.updateRules(fromSource: source,
                                     forContentBlocker: Constants.contentBlockerIdentifier,
                                     completion: { [weak self] result in
                                        switch result {
                                        case .success:
                                            self?.onExtensionReloadCompleted(nil)
                                        case let .failure(error):
                                            self?.onExtensionReloadCompleted(error)
                                        }
        })
    }

    private func getLocalFilterListLanguage() -> FilterlistLanguage {
        let defaultCode = Locale.preferredLanguages[0]
        switch defaultCode.prefix(2) {
        case "de":
            return .german
        case "fr":
            return .french
        case "nl":
            return .dutch
        case "es":
            return .spanish
        case "zh":
            return .chinese
        case "it":
            return .italian
        case "ru":
            return .russian
        case "en":
            return .english
        default:
            return .english
        }
    }

    private func storeAAState(_ isOn: Bool) {
        groupDefaults?.set(isOn, forKey: Constants.acceptableAdsEnabled)
    }

    private func storeSelectedLanguage(_ language: FilterlistLanguage) {
        groupDefaults?.set(language.rawValue, forKey: Constants.selectedFilterList)
    }
}
