# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.1] - 2019-12-03
### Added
* UI for analytics opt-in
* Logic for Fabric event tracking & crash reporting
### Changed
* Updated Filter Lists

## [2.1] - 2019-11-22
### Added
* Localization into Chinese, Dutch, French, German, Italian, Russian and Spanish
* Added regional filterlists for Chinese, Dutch, French, German, Italian, Russian and Spanish locales
* Dark Mode support
* System Accent Color support
* Groundwork for Firebase integration
### Changed
* Onboarding to presented as modal window
### Removed
* Google+ integration
### Fixed
* Whitelist input validation & duplication errors
* Missing NSWindow title
* Minor UI tweaks
* Minor strings tweak

## [2.0.6] - 2019-05-01
### Changed
* Updated Filter Lists

## [2.0.5] - 2019-04-08
### Changed
* Updated Filter Lists

## [2.0.4] - 2018-12-03
### Changed
* Updated Filter Lists

## [2.0.3] - 2018-10-02
### Changed
* Updated Filter Lists

## [2.0.2] - 2018-09-27
### Added
##### Host App
* App now loads new filter lists between app updates
### Fixed
##### Host App
* Filter list not loading on initial install in some instances
* Incorrect menu tab highlighting on macOS Sierra
* Pre-roll ads & in-video ads on youtube.com
### Changed
##### Host App
* Deployment target to 10.12.6 from 10.12
* On-boarding for legacy users to see toolbar installation procedure
* Styling of on-boarding slides
##### Toolbar & Content Blocker Extensions
* Toolbar icon hover text
* Toolbar extension Description
* ABP Content Blocker & Toolbar extensions names:
  * Adblock Plus is now ABP
  * Adblock Plus Icon is now ABP Control Panel
  * On-boarding assets updated to reflect new naming
  * On-boarding copy updated to reflect new naming

## [2.0.1] - 2018-09-19
### Fixed
##### Host App
- Now updates UI when whitelist updated from Toolbar

## [2.0.0] - 2018-09-19
### Added
##### Host App
* On-boarding instructions
* Acceptable Ads toggle
* User controlled whitelisting
* Help Page with support/social links
##### Safari Toolbar Extension
* User controlled whitelisting
##### Safari Content Blocker Extension
* Automated removal of legacy ABP extension